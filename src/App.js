import './App.css';
import React, {useEffect, useState} from 'react'
import axios from 'axios'
import LazyLoad from 'react-lazyload'
import InfiniteScroll from "react-infinite-scroll-component";

const headers = {
	"x-rapidapi-key": "2a0a0e208dmshd40ea208f4f0a69p19f180jsna0f20561c2ec",
	"x-rapidapi-host": "asos2.p.rapidapi.com",
	"useQueryString": true
}
function App() {
  const [productList, setProductList] = useState([])
  const [offset, setOffset] = useState(0)

  const fetchUsersWithFetchAPI = (offset) => {
    const URL = `https://asos2.p.rapidapi.com/products/v2/list?offset=${offset}&categoryId=4209&limit=5&store=US&country=US&currency=USD&sort=freshness&lang=en-US`
    setOffset(offset)
    axios.get(URL, {headers})
      .then(response => {
        if (response.data && response.data.products) {
          setProductList(response.data.products)
        }
      })  
      .catch(e => {
        console.log('error', e)
      });

  }

  useEffect(() => {
    fetchUsersWithFetchAPI(0)
  }, [])

  return (
    <div className="wrapper">
      <InfiniteScroll
          dataLength={productList.length}
          next={() => fetchUsersWithFetchAPI(offset + 1)}
          hasMore={true}
          loader={<h4>Loading...</h4>}
          scrollThreshold={0.5}
        >
          <div className="widget-list">
            {productList && productList.length ? productList.map((item, i) => {
              return (
                <LazyLoad>
                  <div className="card" key={i}>
                    <img src={`http://${item.imageUrl}`} alt="Avatar" style={{height: 300,marginTop: 19, width: 576}}  alt='prodct'/>
                    <div className="container">
                      <h4><b>{item.name}</b></h4> 
                      <p>{item.brandName}</p> 
                    </div>
                  </div>
                </LazyLoad>
              );
            }) : null}
          </div>
        </InfiniteScroll>
    </div>
  )
}

export default App;
